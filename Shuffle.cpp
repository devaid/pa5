#include<iostream>
#include<string>
#include<stdexcept>
#include<iomanip>
#include"List.h"

using namespace std;
using std::setw;

void shuffle(List& D) {
    D.moveFront();
    int halfway = D.length() / 2;
    List *l = new List();
    while (halfway > 0) {
        D.moveNext();
        halfway--;
    }
    ListElement el = D.moveNext();
    while (el > -1) {
        l->insertBefore(el);
        el = D.moveNext();
    }
    D.moveFront();
    l->moveFront();
    ListElement el2 = l->moveNext();
    while(el2 > -1) {
        D.insertBefore(el2);
        D.moveNext();
        el2 = l->moveNext();
    }
    D.cleanup();
}

int numberOfDigits(int i) {
   int count = 0;
   while(i != 0) {
      i = i / 10;
      count++;
   }
   return count;
}

int main(int argc, char** argv) {
  int times = stoi(argv[1]);
  cout << "deck size       shuffle count" << endl << "------------------------------" << endl;
  for (int i = 1; i <= times; i++) {
      List *l = new List();
      for (int j = 0; j < i; j++) {
          l->insertBefore(j);
      }
      l->moveFront();
      int shuffleCount = 1;
      List *m = new List(*l);
      shuffle(*l);
      while (!l->equals(*m)) {
          shuffle(*l);
          shuffleCount++;
      }
      cout << i << setw(15 - numberOfDigits(i) + numberOfDigits(shuffleCount)) << shuffleCount << endl;
  }
  return 0;
};
